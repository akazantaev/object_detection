from transformers import DetrImageProcessor, DetrForObjectDetection

processor = DetrImageProcessor.from_pretrained("facebook/detr-resnet-50", revision="no_timm")
model = DetrForObjectDetection.from_pretrained("facebook/detr-resnet-50", revision="no_timm")

processor.to_json_file("../detr-resnet-50/processor.json")
model.save_pretrained("../detr-resnet-50/model")
