from transformers import DetrImageProcessor, DetrForObjectDetection
import torch
from PIL import Image
import cv2
import numpy as np

def box_label(image, box, label='', color=(128, 128, 128), txt_color=(255, 255, 255)):
    lw = max(round(sum(image.shape) / 2 * 0.003), 2)
    p1, p2 = (int(box[0]), int(box[1])), (int(box[2]), int(box[3]))
    cv2.rectangle(image, p1, p2, color, thickness=lw)
    if label:
        tf = max(lw - 1, 1)
        w, h = cv2.getTextSize(label, 0, fontScale=lw / 3, thickness=tf)[0] 
        outside = p1[1] - h >= 3
        p2 = p1[0] + w, p1[1] - h - 3 if outside else p1[1] + h + 3
        cv2.rectangle(image, p1, p2, color, -1, cv2.LINE_AA)
        cv2.putText(
            image,
            label,
            (p1[0], p1[1] - 2 if outside else p1[1] + h + 2),
            0,
            lw / 3,
            txt_color,
            thickness=tf,
            lineType=cv2.LINE_AA,
        )


image = Image.open("./images/stop.jpg")

processor = DetrImageProcessor.from_pretrained("../detr-resnet-50/processor.json")
model = DetrForObjectDetection.from_pretrained("../detr-resnet-50/model/")

inputs = processor(images=image, return_tensors="pt")
outputs = model(**inputs)

target_sizes = torch.tensor([image.size[::-1]])
results = processor.post_process_object_detection(outputs, target_sizes=target_sizes, threshold=0.9)[0]

for score, label, box in zip(results["scores"], results["labels"], results["boxes"]):
    box = [round(i, 2) for i in box.tolist()]
    result = {
        "label": model.config.id2label[label.item()],
        "left_top": (box[0], box[1]),
        "right_bottom": (box[2], box[3]),
        "confidence": round(score.item(), 3),
    }
    print(result)
    
    image = np.asarray(image).astype(np.uint8)
    
    box_label(image, box, model.config.id2label[label.item()])

image = Image.fromarray(image)
image.save("example.jpg")