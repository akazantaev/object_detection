# Локальная сборка и запуск

```bash
mkdir detr-resnet-50
cd examples
python3 ./model.py
cd ..
docker build -t object_detection .
docker-compose up
```
